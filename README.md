# Yeorumi
Discord bot by Ferko#9717.

Package dependencies are installed by running `make deps`

## Features
* Role logging

# Requirements
* A `config.json` file. Use `config.json.example` as the basis.

# Installation

```
make deps
go install
```

# Contact
* Discord: Ferko#9717
