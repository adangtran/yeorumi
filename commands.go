package main

import (
	"fmt"
	"strings"

	"github.com/bwmarrin/discordgo"
)

type CmdType func(s *discordgo.Session, m *discordgo.MessageCreate)

// Ping command replies with "Pong!"
func ping(s *discordgo.Session, m *discordgo.MessageCreate) {
	s.ChannelMessageSend(m.ChannelID, "Pong!")
}

func CommandDispatch(s *discordgo.Session, m *discordgo.MessageCreate) {
	cmdMap := map[string]CmdType{
		"ping": ping,
	}
	cmd := prefix_regex.FindStringSubmatch(m.Content)
	if cmd != nil {
		// Command must be first match
		cmdStr := strings.ToLower(cmd[1])
		fmt.Printf("Command: %s\n", cmdStr)
		if _, ok := cmdMap[cmdStr]; ok {
			go cmdMap[cmdStr](s, m)
		} else {
			s.ChannelMessageSend(m.ChannelID, GetResp("cmd:unknown", cmdStr))
		}
	}
}
